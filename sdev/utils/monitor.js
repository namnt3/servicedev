var $ = module.exports = {};

var scopes = {};

function scopeData(name) {
    if (scopes[name]) {
        return scopes[name];
    }
    scopes[name] = {
        call: 0,
        process: 0,
        rps: 0,
        lwt: 0,
        wt: 0
    };
    return scopes[name];
}

$.scope = function (name) {
    var start = process.hrtime()[1];
    var scope = scopeData(name);
    scope.call++;
    scope.process++;
    scope.rps++;
    return {
        end: function () {
            scope.lwt = process.hrtime()[1] - start;
            scope.wt += scope.lwt;
            scope.process = scope.process - 1;
        }
    };
};

$.init = function (info) {
    if (process.send) {
        setInterval(function () {
            var msg = {
                cmd: "init",
                service: info.name,
                port: info.port,
                scope: scopes
            };
            process.send(msg);
            for (var key in scopes) {
                if (scopes.hasOwnProperty(key)) {
                    scopes[key].rps = 0;
                }
            }
        }, 1000);
    }
};
