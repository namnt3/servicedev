exports = module.exports = html;

function html(route, client, input, params, next) {
    if (params && params[0] && (typeof params[0] == "function")) {
        try {
            input.unshift(client);
            params[0].apply(client, input);
        } catch (err) {
            console.error(err.stack);
            client.cb("SYS_ERR>" + err);
        }
    } else {
        console.error("SYS_ERR> in route '" + route + "', there is an invalid Html body");
        client.cb("SYS_ERR>Internal error");
    }
}