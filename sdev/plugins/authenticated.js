exports = module.exports = authenticated;

function authenticated(route, client, input, params, next) {
    if (client.userId) {
        next();
        return;
    }
    client.cb("ACCESS_DENIED");
}