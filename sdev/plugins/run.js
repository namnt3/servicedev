exports = module.exports = run;

function run(route, client, input, params, next) {

    if (params && params[0] && (typeof params[0] == "function")) {
        //input.push(function (err, value) {
        //    client.cb(err, value);
        //});
        try {
            input.unshift(client);
            params[0].apply(params[0], input);
        } catch (err) {
            client.cb(err);
        }
    } else {
        console.error("[ERROR] SYS> in route '" + route + "', there is an invalid Run body");
        client.cb("ERROR>Invalid Run body");
    }
}