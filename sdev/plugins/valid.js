var async = require("async");
var fs = require("fs");
var jsep = require("jsep");
var jexl = require("jexl");

exports = module.exports = valid;

function getVaraibles(str) {
    var tree = jsep(str);
    var variables = {};

    function exporting(node) {
        if (node.argument) {
            exporting(node.argument);
        }
        if (node.left) {
            exporting(node.left);
            exporting(node.right);
        }
        if (node.name) {
            variables[node.name] = 1;
        }
    }

    if (tree.type === "Compound") {
        throw new Error("Invalid expression: '" + str + "'");
    }

    exporting(tree);

    return variables;
}

sdev.context.validators = {};

if (fs.existsSync(sdev.context.appDir + "/validators.js")) {
    console.info("[VALID] read validators");
    try {
        sdev.context.validators = require(sdev.context.appDir + "/validators.js");
    } catch (e) {
        console.error(e.stack);
        process.exit(1);
    }
}

function isTrue(expression, fieldValue, cb) {
    try {
        var vars = getVaraibles(expression);
    } catch (e) {
        cb(e);
    }
    async.eachOfSeries(Object.keys(vars), function(validName, index, next) {
        if (sdev.context.validators[validName]) {
            try {
                sdev.context.validators[validName](fieldValue, function(err, value) {
                    if (err) {
                        return next("evaluate(" + validName + ") failed, " + err);
                    }
                    vars[validName] = value;
                    next();
                });
            } catch (e) {
                next(e);
            }
        } else {
            next("no validator named '" + validName + "'");
        }
    }, function(err) {
        if (err) {
            if(err instanceof Error) {
                return cb(err);
            }
            return cb("Expression[" + expression + "], " + err)
        }

        try {
            jexl.eval(expression, vars, function (err, r) {
                if(err) {
                    return cb("Can't evaluate expression[" + expression + "]. " + err);
                }
                cb(null, r);
            });
        } catch (e) {
            return cb("Can't evaluate expression[" + expression + "]. " + e);
        }
    });
}

function getValue(path, expression, value, cb) {
    if (typeof expression === "string") {
        if (expression.trim() === "") {
            return cb(null, value);
        }
        isTrue(expression, value, function(err, result) {
            if (err) {
                if(err instanceof Error) {
                    return cb(err);    
                }
                return cb(path + ", " + err);
            }
            if(result) {
                cb(null, value);
            } else {
                return cb(path + " required [" + expression + "], but value =  [" + value + "]");
            }
            
        });
        return;
    }
    if (typeof expression === "function") {
        expression(value, function(err, value) {
            if (err) {
                return cb(path + ", " + err);
            }
            cb(null, value);
        });
        return;
    }
    if (typeof expression === "object") {
        if (typeof value !== "object") {
            return cb(path + " must be an object");
        }
        var obj = expression;
        //var temp = {}; //old
        var temp = value;
        async.eachOfSeries(obj, function(expression, key, next) {
            getValue(path + "." + key, expression, value[key], function(err, result) {
                if (err) {
                    return next(err);
                }
                temp[key] = result;
                next();
            });
        }, function(err) {
            cb(err, temp);
        });
        return;
    }
}


function valid(route, client, input, pluginParams, next) {
    if (pluginParams && pluginParams.length > 0) {
        if (!input || input.length === 0) {
            return client.cb("INVALID>Missing input[0]");
        }

        async.eachOfSeries(pluginParams, function(pluginParam, index, forReachNext) {
            if (index < input.length) {
                var inputItem = input[index];

                getValue("Inputs[" + index + "]", pluginParam, inputItem, function(err, value) {
                    input[index] = value;
                    forReachNext(err);
                });
            } else {
                forReachNext("INVALID>Missing inputs[" + index + "]");
            }
        }, function(error) {
            if (error) {
                if (error instanceof Error) {
                    console.error(error.stack);
                    client.cb("SYS_ERR>Validation error.");
                } else {
                    client.cb("INVALID>" + error);
                }
            } else {
                next();
            }
        });
    } else {
        return next();
    }

}
