var $ = exports.path = function (path) {
    return new Builder(path);
};

var Builder = function (path) {
    var selft = this;
    selft.path = path;
    selft.validators = [];
    selft.clientRoles = [];
};
Builder.prototype.valid = function () {
    this.validators = arguments;
    return this;
};
Builder.prototype.roles = function () {
    this.clientRoles = arguments;
    return this;
};
Builder.prototype.func = function (f) {
    this.func = f;
    if(GLOBAL.sdev.paths[this.path]) {
        throw new Error("Path '" + this.path + "' already exists");
    }
    GLOBAL.sdev.paths[this.path] = this;
    console.info("\t " + this.path);
};
Builder.prototype.stream = function (s) {
    this.stream = s;
    if(GLOBAL.sdev.paths[this.path]) {
        throw new Error("Path '" + this.path + "' already exists");
    }
    GLOBAL.sdev.paths[this.path] = this;
    console.info("\t " + this.path + " -----");
};