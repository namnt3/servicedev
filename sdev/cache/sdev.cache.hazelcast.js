var Hazelcast = require('hazelcast-client');

var $ = module.exports = Cache;

function Cache(options) {
    var self = this;
    self.options = options;
    self.hzConfig = new Hazelcast.Config.ClientConfig();
    self.hzConfig.groupConfig.name = "sdev";
    self.hzConfig.groupConfig.password = "servicedev";
    self.hzConfig.networkConfig.addresses = [{host: "localhost", port: 5701}];
    self.hzConfig.properties['hazelcast.logging'] = "off";

    if (!options.region || !options.service || !options.instance) {
        throw new Error("Invalid options can't be empty");
    }
    self.key = options.region + "^" + options.service + "^" + options.instance;
}

Cache.prototype.start = function (cb) {
    var self = this;

    try {
        Hazelcast.Client
            .newHazelcastClient(self.hzConfig)
            .then(function (client) {
                self.client = client;
                self.map = client.getMap(self.key);
                cb();
            });
    } catch (e) {
        cb("HAZELCAST>" + e);
    }
};

Cache.prototype.put = function (key, value, ttl, cb) {
    var self = this;
    if (!key) {
        return cb("CACHE> Key can't be empty");
    }
    if (typeof ttl == "function") {
        cb = ttl;
        ttl = 3600000;
    } else {
        if (ttl > 86400000) {
            return cb("CACHE> ttl can't be greater than 86400000");
        }
    }

    try {
        console.log("PUT : " + key);
        self.map.put(key, value, ttl).then(function () {
            cb(null, value);
        });
    } catch (e) {
        cb(e);
    }
};

Cache.prototype.get = function (key, loader, ttl, cb) {
    var self = this;

    if (!key) {
        return cb("CACHE> Key can't be empty");
    }

    if (!cb) {
        cb = ttl;
        ttl = 3600;
        if (!cb) {
            cb = loader;
            loader = function (cb) {
                cb();
            }
        }
    }

    try {
        self.map.get(key).then(function (value) {
            if (value) {
                return cb(null, value);
            }

            loader(function (err, value) {
                if (err) {
                    return cb("LOADER>" + err);
                }
                if (value === null || value === undefined) {
                    cb();
                } else {
                    self.put(key, value, ttl, cb);
                }
            });
        });
    } catch (e) {
        cb(e);
    }
};
