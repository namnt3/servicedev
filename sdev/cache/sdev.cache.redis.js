var Redis = require('ioredis');
var $ = module.exports = Cache;

function Cache(db, addresses) {
    var self = this;
    self.remotes = [];
    self.db = db;
    for (var index in addresses) {
        try {
            var x = addresses[index].split(":");
            self.remotes.push({
                host: x[0],
                port: x[1]
            });
        } catch (e) {
            console.error("CONFIG_ERR> wrong redis configuration. " + JSON.toString(addresses));
            return;
        }
    }

}
Cache.prototype.start = function(cb) {
    var self = this;

    self.redis = new Redis.Cluster(self.remotes, {
        redisOptions: {
            family: 4,
            password: 'servicedev',
            db: 0
        }
    });
    self.redis.on('error', function(err) {
        console.log("REDIS CONNECT error " + err);
        console.log('node error', err.lastNodeError);
    });
    cb();
};

/**
 *	put(key, value, [ttl], cb);
 **/
Cache.prototype.put = function(key, value, ttl, cb) {
    if (!key) {
        return cb("CACHE> Key can't be empty");
    }
    if (typeof ttl == "function") {
        cb = ttl;
        ttl = 3600;
    } else {
        if (ttl > 86400) {
            return cb("CACHE> ttl can't be greater than 68400");
        }
    }

    var str;

    if (typeof value == "string") {
        str = "S" + value;
    } else if (typeof value == "number") {
        str = "N" + value;
    } else if (typeof value == "object") {
        str = "O" + JSON.toString(value);
    } else {
        return cb("CACHE>unsupported data type: " + (typeof value));
    }

    self.redis.set(key, str, function(err) {
        if (err) {
            return cb("REDIS>" + err);
        }
        self.redis.expire(key, ttl, function(err) {
            if (err) {
                return cb("REDIS>" + err);
            }
            cb(null, value);
        });
    });
};

Cache.prototype.get = function(key, loader, ttl, cb) {
    var self = this;

    if (!key) {
        return cb("CACHE> Key can't be empty");
    }

    if (!cb) {
        cb = ttl;
        ttl = 3600;
        if (!cb) {
            cb = loader;
            loader = function(cb) {
                cb();
            }
        }
    }

    self.redis.get(key, function(err, str) {
        if (err) {
            return cb("REDIS>" + err);
        }

        console.log("VALUE: " + str);

        if (str) {
            value = str.substring(1);
            switch (str.charAt(0)) {
                case "S":
                    break;
                case "N":
                    value = Number(value);
                    break;
                case "O":
                    value = JSON.parse(value);
                    break;
                default:
                    return cb("LOADER>Invalid data from redis:" + value);
            }
            return cb(null, value);
        }
        loader(function(err, value) {
            if (err) {
                return cb("LOADER>" + err);
            }
            self.put(key, value, ttl, cb);
        });
    });
};
