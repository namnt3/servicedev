var args = require('commander');
var path = require("path");
var fs = require("fs");
var http = require('http');
var network = require("./network/network");
var async = require("async");
var aes = require("./utils/aes");
var lessMiddleware = require('less-middleware');
var Pool = require("./network/pool.js");
var monitor = require("./utils/monitor");

var Cache = require("./cache/sdev.cache.hazelcast.js");

var appDir;
args
    .version('0.0.1')
    .option('--controller <string>', 'Controller path')
    .option('-at, --accesstoken <string>', 'Access token')
    .option('-c, --config <string>', 'Config files')
    .action(function (p) {
        appDir = p;
    })
    .parse(process.argv);


if (!appDir) {
    console.error("\nMissing App directory");
    return;
} else {
    appDir = path.resolve(appDir);
    try {
        if (!fs.lstatSync(appDir).isDirectory()) {
            return console.error("Working directory is invalid: " + appDir);
        }
    } catch (e) {
        return console.error("Working directory is invalid: " + appDir);
    }
}

if (args.controller) {
    if (!args.accesstoken) {
        console.error("Missing access token");
    }
    console.info('\nPATH         : ' + appDir);
    console.info("Controller   : " + args.controller);
    console.info("Access token : " + args.controller);
} else if (!args.config) {
    console.error("Missing config");
}


if (GLOBAL.sdev) {
    console.warn("GLOBAL.sdev already exists!!!");
    return;
}
var sdev = GLOBAL.sdev = {};
sdev.context = {
    appDir: appDir,
    routes: {},
    base: [],
    config: {}
};
sdev.defaultPlugins = [];
sdev.plugins = {};
sdev.validators = {};
sdev.functions = {};
sdev.paths = {};
sdev.basedServices = {};
sdev.modules = {};
sdev.services = {};

if (args.config) {
    sdev.context.config = JSON.parse(fs.readFileSync(args.config, 'utf8'));
    console.log(sdev.context.config);
} else {
    //GET CONFIG FORM CONTROLLER;
    console.log("GET CONFIG FORM CONTROLLER");
}

sdev.module = function (name) {
    return sdev.modules[name] || (sdev.modules[name] = {});
};
sdev.setModule = function (name, body) {
    var r = sdev.modules[name] || (sdev.modules[name] = {});
    Object.setPrototypeOf(r, body);
};

sdev.service = function (name) {
    if (!sdev.services[name]) {
        console.error(new Error("No such resource : " + name).stack);
        process.exit(1);
    }
    return sdev.services[name];
};

sdev.addValidators = function () {
    var param;

    for (var i = 0; i < arguments.length; i++) {
        param = arguments[i];

        if (typeof param != "object") {
            throw "Invalid validator: arguments[" + i + "]";
        }
        for (var validatorName in param) {
            if (param.hasOwnProperty(validatorName)) {
                if (typeof param != "function") {
                    throw "Invalid validator: " + validatorName;
                }
                $.validators[validatorName] = param[validatorName];
                console.log("[Validators] " + validatorName);
            }
        }
    }
};

//sdev.path = require("./sdev.mock.js").path;


function createServiceBuilder(model) {
    return function (name) {
        if (name == null || name === "") {
            name = "_";
        }
        var pluginChain = {};
        model[name] = [];
        Object.keys(sdev.plugins).forEach(function (plugin) {
            pluginChain[plugin] = function () {
                model[name].push({
                    pluginName: plugin,
                    params: arguments
                });
                return pluginChain;
            };
        });

        return pluginChain;
    };
}

function readRoutesDirSync(dir, model) {
    var files = fs.readdirSync(dir);

    for (var index in files) {
        var file = files[index];
        var filePath = dir + "/" + file;
        var stat = fs.lstatSync(filePath);
        var fileInfo = path.parse(filePath);

        if (stat.isDirectory()) {
            if (model[fileInfo.name]) {
                throw new Error("Route[" + fileInfo.name + "] name has been defined before, maybe you create a file has a same name of folder.")
            }
            model[fileInfo.name] = {};
            readRoutesDirSync(filePath, model[fileInfo.name]);
        } else if (stat.isFile()) {
            if (fileInfo.ext === ".js") {
                if (fileInfo.name === "_") {
                    sdev.route = createServiceBuilder(model);
                    require(filePath);
                    delete sdev.route;
                } else {
                    model[fileInfo.name] = {};
                    sdev.route = createServiceBuilder(model[fileInfo.name]);
                    require(filePath);
                    delete sdev.route;
                }
            }
        }
    }
}


function startCache(cb) {
    console.log("[CACHE] start");
    sdev.cache = new Cache(sdev.context.config);
    sdev.cache.start(cb);
}

function loadPlugins(cb) {
    sdev.plugins.valid = require("./plugins/valid");
    sdev.plugins.run = require("./plugins/run");
    sdev.plugins.stream = require("./plugins/stream");
    sdev.plugins.html = require("./plugins/html");
    sdev.plugins.authenticated = require("./plugins/authenticated");

    var dir = appDir + "/plugins";
    if (fs.existsSync(dir)) {
        console.info("[APP] read plugins");

        var files = fs.readdirSync(dir);

        for (var index in files) {
            var file = files[index];
            var filePath = dir + "/" + file;
            var stat = fs.lstatSync(filePath);
            var fileInfo = path.parse(filePath);

            if (stat.isDirectory()) {
                console.log("Unsupported reading directory in /plugins folder")
            } else if (stat.isFile()) {
                if (fileInfo.ext === ".js") {
                    console.log("[PLUGIN] reading " + fileInfo.name);
                    sdev.plugins[fileInfo.name] = require(filePath);
                }
            }
        }
    }

    cb();
}

function doGet(addresses, path, cb) {
    function getAddresses(address, cb) {
        console.log("POST TO " + address);
        var options = {
            host: address.split(":")[0],
            port: address.split(":")[1],
            path: path,
            method: 'GET'
        };

        var req = http.request(options, function (res) {
            if (res.statusCode != 200) {
                //return getPeers(++addressIndex);
            }
            var body = '';
            res.on('data', function (d) {
                body += d;
            });
            res.on('end', function () {
                try {
                    var parsed = JSON.parse(body);
                    cb(null, parsed);
                } catch (e) {
                    cb("PARSE_ERR>" + e);
                }
            });
        });

        req.on('error', function (err) {
            cb(err);
        });

        req.end();
    }

    async.forEachSeries(addresses, function (address, next) {
        getAddresses(address, function (err, arr) {
            if (err) {
                return next();
            }
            cb(null, arr);
        });
    }, function (err) {
        if (err) {
            cb("NO_SERVER");
        }
    });
}

function loadBasedServices(cb) {
    console.info("[LOAD BASED SERVICES]");

    async.eachOfSeries(sdev.context.config.resources, function (value, name, next) {
        console.info("Load service " + name);
        sdev.services[name] = {};
        var pool = sdev.services[name].__pool = new Pool(value.servers);
        pool.setAccessToken(value.at);

        pool.connect(function (err) {
            if (err) {
                return next(err);
            }
            doGet(value.servers, "/__desc", function (err, desc) {
                if (err) {
                    return next(err);
                }
                function build(obj, desc, route) {
                    Object.keys(desc).forEach(function (key) {
                        var value = desc[key];
                        if (Array.isArray(value)) {
                            obj[key] = function () {
                                var args = Array.prototype.slice.call(arguments);
                                var callback = args.pop();
                                var src = undefined;
                                if (typeof callback === "string") {
                                    src = callback;
                                    callback = args.pop();
                                }
                                pool.call(route + "/" + key, args, src, callback);
                            }
                        } else if (key === "_") {
                            obj[key] = {};
                            build(obj, value, route);
                        } else {
                            obj[key] = {};
                            build(obj[key], value, route + "/" + key);
                        }
                    });
                }

                sdev.services[name] = {};
                build(sdev.services[name], desc.routes, "");
                next();
            });
        });
    }, function (err) {
        console.log(JSON.stringify(sdev.services));
        cb(err);
    });
}

function initApp(cb) {
    if (fs.existsSync(appDir + "/init.js")) {
        console.info("[INIT APPS]");
        try {
            var init = require(appDir + "/init.js");
            if (typeof init !== "function") {
                return console.error("Invalid init file.");
            }
            return init(cb);
        } catch (e) {
            cb(e);
        }
        return;
    }
    cb();
}

function startServer(region, service, instance, networkAddress) {
    var arr = networkAddress.split("/");
    if (arr.length !== 2) {
        return console.error("Wrong network address: " + address);
    }
    var server = sdev.server = new network.Server(region, service, instance, arr[0], arr[1]);

    server.on('addNode', function (info) {
        console.info("[NETWORK] %sps,  + %s", server.peers.list.length, info);
    });
    server.on('removeNode', function (info) {
        console.info("[NETWORK] %sps,  - %s", server.peers.list.length, info);
    });


    var mustacheExpress = require('./libs/mustache-express/mustache-express.js');

    server.express.engine('mustache', mustacheExpress());

    server.express.set("view engine", "mustache");
    server.express.set('views', sdev.context.appDir + '/pages');
    // server.express.register(".mustache", require('mustache'));
    server.express.use("/public/css", lessMiddleware(sdev.context.appDir + '/public/css'));
    server.express.use("/public", require("express").static(sdev.context.appDir + '/public'));
    server.express.use("/bower_components", require("express").static(sdev.context.appDir + '/bower_components'));

    server.express.use("/__desc", function (req, res, next) {
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(sdev.context));
    });

    server.express.use("/__servers", function (req, res, next) {
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(server.peers.list));
    });

    var mustache = require('mustache');
    //mustache.tags = ['{(', ')}'];
    var templates = {
        cache: {},
        render: function (file, scope) {
            var cache = templates.cache;
            if (!cache[file]) {
                cache[file] = fs.readFileSync(file, 'utf8');
            }
            return mustache.render(cache[file], scope);
        }
    };
    server.express.use("/__AngularService", function (req, res, next) {
        var text = templates.render(__dirname + "/templates/AngularService.txt", {
            module: req.query.module,
            routes: JSON.stringify(sdev.context.routes).replace(" ", "")
        });
        res.setHeader('Content-Type', 'text/javascript; charset=UTF-8');
        res.send(text);
    });

    var sv = sdev.context.routes;
    server.express.use(function (req, res, next) {
        var userId = null;
        try {
            userId = aes.decrypt(req.cookies.at);
        } catch (e) {
        }

        var scope = monitor.scope(req.path);
        var client = {
            userId: userId,
            replied: false,
            cb: function (err, obj) {
                updateCookie();

                if (client.replied) {
                    throw new Error("Client.cb already called");
                }
                client.replied = true;

                scope && scope.end();
                scope = null;

                res.setHeader('Content-Type', 'application/json');
                if (err) {
                    if (err instanceof Error) {
                        console.error(err.stack);
                    }
                    res.send({
                        e: err
                    });
                } else {
                    res.send({
                        b: obj
                    });
                }
            },
            req: req,
            res: res,
            render: function () {
                updateCookie();
                res.render.apply(res, arguments);

                scope && scope.end();
                scope = null;
            },
            redirect: function () {
                updateCookie();
                res.redirect.apply(res, arguments);

                scope && scope.end();
                scope = null;
            }
        };

        function updateCookie() {
            if (client.userId) {
                res.cookie("at", aes.encrypt("" + client.userId));
            } else {
                res.cookie("at", "");
            }
        }

        var input = [];

        function exportInput(values, tryParse) {
            for (var k = 0; values[k]; k++) {
                var obj = values[k];
                try {
                    input.push(JSON.parse(obj));
                } catch (e) {
                    input.push(obj);
                }
            }
            if (values[0] === undefined) {
                var obj = {};
                Object.keys(values).forEach(function (key) {
                    if (tryParse) {
                        try {
                            obj[key] = JSON.parse(values[key]);
                        } catch (e) {
                            //console.error(e.stack);
                            obj[key] = values[key];
                        }
                    } else {
                        obj[key] = values[key];
                    }

                });
                if (Object.keys(obj).length > 0) {
                    input.push(obj);
                }
            }
        }

        exportInput(req.query, true);
        if (req.method === "POST" || req.method === "PUT" || req.method === "DELETE") {
            exportInput(req.body);
        }


        console.log("INPUT : " + JSON.stringify(input));
        // if (input.length === 0) {
        //     input.push(req.query);   

        var path = req.path.split("/");

        var endPoint = sv;
        var route;

        for (var i = 1; i < path.length; i++) {
            route = path[i];

            if (endPoint[route]) {
                endPoint = endPoint[route];
            } else if (route === "") {
                endPoint = endPoint._;
                break;
            } else {
                endPoint = null;
                break;
            }
        }

        if (endPoint && !Array.isArray(endPoint)) {
            endPoint = endPoint._;
        }

        if (endPoint) {
            if (Object.prototype.toString.call(endPoint) === '[object Array]') {
                async.eachSeries(endPoint, function (p, next) {
                    try {
                        sdev.plugins[p.pluginName].apply({}, [route, client, input, p.params, next]);
                    } catch (e) {
                        console.error(e.stack);
                    }
                });
            } else {
                client.cb(JSON.stringify(endPoint));
            }

        } else {
            client.cb("NO_SERVICE>No such services");
        }
    });

    server.on("newSocketClient", function (con) {
        var USER_ID = null;
        con.on('message', function (message) {
            var msg;
            try {
                msg = JSON.parse(message);
            } catch (e) {
                con.send(JSON.stringify({
                    e: "INVALID_JSON"
                }));
                console.error(message);
                return;
            }

            if (msg.at) {
                try {
                    USER_ID = aes.decrypt(msg.at);
                } catch (e) {
                }
            }

            var client = {
                ws: con,
                userId: USER_ID,
                replied: false,
                src: msg.s,
                cb: function (err, obj) {
                    if (client.replied) {
                        throw new Error("Client.cb already called");
                    }
                    client.replied = true;

                    var result = {};

                    if (USER_ID) {
                        result.at = aes.encrypt(USER_ID);
                    }

                    if (err) {
                        result.e = err;
                    }

                    if (obj) {
                        result.b = obj;
                    }

                    if (msg.i) {
                        result.i = msg.i;
                    }

                    try {
                        con.send(JSON.stringify(result));
                    } catch (e) {
                        console.warn("Client closed");
                    }
                },
                render: function () {
                    console.error("Unsupported render in websocket connection");
                },
                redirect: function () {
                    console.error("Unsupported redirect in websocket connection");
                }
            };

            var path = msg.r.split("/");

            var endPoint = sv;
            var route;

            for (var i = 1; i < path.length; i++) {
                route = path[i];

                if (endPoint[route]) {
                    endPoint = endPoint[route];
                } else if (route === "") {
                    endPoint = endPoint._;
                    break;
                } else {
                    endPoint = null;
                    break;
                }
            }

            if (endPoint && !Array.isArray(endPoint)) {
                endPoint = endPoint._;
            }

            if (endPoint) {
                if (Object.prototype.toString.call(endPoint) === '[object Array]') {
                    async.eachSeries(endPoint, function (p, next) {
                        try {
                            sdev.plugins[p.pluginName].apply({}, [route, client, msg.p, p.params, next]);
                        } catch (e) {
                            console.error(e.stack);
                        }
                    });
                } else {
                    client.cb("INVALID_SERVICE> " + endPoint);
                }

            } else {
                client.cb("NO_SERVICE>No such services");
            }
        });
        con.on('close', function () {
            console.log("Client closed");
        });
    });


    server.start(function (err, info) {
        if (err) throw err;
        console.info("[NETWORK] listening %s:%d", info.host, info.port);

        monitor.init({
            name: "MyService",
            port: info.port
        });
    });
}


startCache(function (error) {
    if (error) {
        console.error(error);
        process.exit();
    }

    loadBasedServices(function (error) {
        if (error) {
            console.error(error);
            process.exit();
        }

        loadPlugins(function (error) {
            if (error) {
                console.error(error);
                process.exit();
            }

            initApp(function (error) {
                if (error) {
                    console.error(error);
                    process.exit();
                }

                if (fs.existsSync(appDir + "/routes")) {
                    console.info("[APP] read routes");
                    readRoutesDirSync(appDir + "/routes", sdev.context.routes);
                }

                console.info("[OPENING]");
                if (args.controller) {
                    console.log("UNDER CONSTRUCTION");
                } else {
                    var c = sdev.context.config;
                    startServer(c.region, c.service, c.instance, c.network);
                }
            });
        });
    });
});
