//var SocketClient = require('sockjs-client-ws');
var http = require("http");
var NodeCache = require("node-cache");
var util = require("util");
var WebSocket = require('ws');
var async = require('async');

module.exports = Pool;

function Pool(addresses, ops) {
    ops = ops || {};

    ops.maxCon = ops.maxCon || 3;
    ops.timeout = ops.timeout || 5;

    var self = this;

    self.addresses = addresses;
    self.maxCon = ops.maxCon;
    self.connections = [];
    self.curCon = -1;
    self.indexSeed = 0;
    self.realConnection = 0;
    self.timeout = ops.timeout;
    self.accessToken = undefined;
    self.cache = new NodeCache({
        stdTTL: self.timeout,
        checkperiod: self.timeout + (self.timeout / 2),
        useClones: false
    });
    self.cache.on("expired", function (key, value) {
        value("TIMEOUT> over " + self.maxCon + "s, but no reply");
    });

    self.conIdSeed = 0;
    self.creatingConneciton = false;

    //for (var i = 0; i < ops.maxCon; i++) {
    //    self.connections.push(1);
    //}
}

var EventEmitter = require('events').EventEmitter;
util.inherits(Pool, EventEmitter);

Pool.prototype.onMessage = function (message) {
    var self = this;
    var msg = null;
    try {
        msg = JSON.parse(message);
    } catch (e) {
        if(message === "NEW_SERVER") {
            if(self.realConnection < self.maxCon) {
                console.log("NEW_SERVER => create more connection");
                self.connect(function () {});
            } else {
                console.log("REAL CONNECTION LIMITED: " + self.realConnection);
            }
            return;
        }
        console.error("Sever send invalid json msg: " + message);
        return;
    }

    if (msg.i) {
        self.cache.get(msg.i, function (err, value) {
            if (err) {
                console.error(err);
                return;
            }
            var delNum = self.cache.del([msg.i]);
            if (delNum > 0) {
                value(msg.e, msg.b);
            } else {
                //console.log("KEY arealdy removed");
            }
        });
    }else {
        self.emit("message", msg);
    }
};
Pool.prototype.setAccessToken = function (at) {
    this.accessToken = at;
};

Pool.prototype.connect = function (cb) {
    var self = this;
    if(self.creatingConneciton) {
        return;
    }
    self.creatingConneciton = true;

    function getPeers(cb) {
        function getAddresses(address, cb) {
            console.log("POST TO " + address);
            var options = {
                host: address.split(":")[0],
                port: address.split(":")[1],
                path: '/__servers',
                method: 'POST'
            };

            var req = http.request(options, function (res) {
                if (res.statusCode != 200) {
                    //return getPeers(++addressIndex);
                }
                var body = '';
                res.on('data', function (d) {
                    body += d;
                });
                res.on('end', function () {
                    console.log("FUCK" + body);
                    try {
                        var parsed = JSON.parse(body);
                        cb(null, parsed);
                    } catch (e) {
                        cb("PARSE_ERR>" + e);
                    }
                });
            });

            req.on('error', function (err) {
                cb(err);
            });

            req.end();
        }

        var found = false;
        async.forEachSeries(self.addresses, function (address, next) {
            getAddresses(address, function (err, arr) {
                if (err) {
                    return next();
                }
                found = true;
                cb(null, arr);
            });
        }, function (err) {
            if (!found) {
                console.log("NO any server");
                cb("NO_SERVER");
            }
        });
    }

    var handler = function (err, result) {
        if (err) {
            console.error(err);
            console.error("Will retry in 3s");
            setTimeout(function () {
                getPeers(handler);
            }, 2000);
            return;
        }
        self.addresses = result;
        console.info("All Address: " + self.addresses);
        createConnection(0, 0);
    };

    function createConnection(index, counter) {
        if (index >= self.addresses.length || counter == self.maxCon) {
            console.log("CREATED %d CONNECTIONS", counter);
            self.creatingConneciton = false;
            cb && cb();
            cb = null;
            return;
        }
        var address = self.addresses[index];
        console.info("Try to connect: " + address);

        var con = new WebSocket("ws://" + address + "/");
        con.ID = ++self.conIdSeed;

        con.on("message", function (data) {
            self.onMessage(data);
        });
        con.on('error', function (e) {
            console.log("FAIL TO CONNECT TO " + address);
            createConnection(index + 1, counter);
        });
        con.on('close', function () {
            console.error("ON CLOSED");
            for (var i = 0; i < self.realConnection; i++) {
                if (self.connections[i].ID === con.ID) {
                    self.connections.splice(i, 1);
                    self.realConnection--;
                    console.log("Connections Number " + self.realConnection + ", array: " + self.connections.length);

                    getPeers(handler);
                }
            }
        });
        con.on("open", function () {
            counter++;

            self.connections.unshift(con);
            self.realConnection = counter;
            setTimeout(function () {
                if (self.realConnection < self.connections.length) {
                    var con = self.connections[self.connections.length - 1];
                    self.connections.pop();
                    con.close();
                }
            }, self.timeout * 1000);

            console.log("CONNECTED TO " + address);
            console.log("Connections Number " + self.realConnection + ", array: " + self.connections.length);
            createConnection(index + 1, counter);
        });
    }

    getPeers(handler);
};
Pool.prototype.getCon = function (cb, tryTime) {
    this.curCon++;
    if (this.curCon >= this.realConnection) {
        this.curCon = 0;
    }
    //console.log(this.connections.length);
    if (this.connections[this.curCon]) {
        cb(undefined, this.connections[this.curCon]);
    } else {
        if (tryTime === undefined) {
            var self = this;
            console.log("TRY");
            setTimeout(function () {
                self.getCon(cb, 3);
            }, self.timeout / 2 * 1000);
        } else if (tryTime > 0) {
            var self = this;
            setTimeout(function () {
                self.getCon(cb, tryTime - 1);
            }, self.timeout / 2 * 1000);
        } else {
            cb("NOT_CONNECTED>");
        }
    }
};
Pool.prototype.call = function (route, params, src, cb) {
    var self = this;

    cb = cb || src;
    src = (typeof src === "string") ? str : undefined;

    this.getCon(function (err, con) {
        if (err) {
            return cb(err);
        }
        var req = {
            i: ++self.indexSeed,
            r: route,
            p: params,
            at: self.accessToken
        };
        if (src) {
            req.s = src;
        }
        self.cache.set(req.i, cb);
        try {
            con.send(JSON.stringify(req));
        } catch (e) {
            setTimeout(function () {
                cb("CON_ERR>" + e);
            }, 1000);
        }
    });
};

Pool.prototype.callSecure = function (route, params, src, cb) {

};

function createConnection(url, cb) {

    var ws = new WebSocket(url);

    cb(null, ws);

}