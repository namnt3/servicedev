var Pool = require("./pool.js");

var pool = new Pool(["servicedev.net:80"]);

pool.connect(function (err) {
    if (err) {
        console.error(err);
        return;
    }
    var k = 0;
    function send() {
        pool.call("/echo", ["HELLO " + (++k)], function (err, msg) {
            k++;
            if (err) {
                console.error(err);
                setTimeout(function (){

                    send();
                }, 2000);
            }
            send();
        });
    }
    send();
    setInterval(function () {
        console.log(k);
        k=0;
        send();
    }, 1000);
});


