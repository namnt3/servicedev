var Discover = require('node-discover');
var util = require("util");

var PeerConnector = exports.PeerConnector = function(myPort, host, key) {
    var self = this;
    var d = this.d = Discover({
        helloInterval: 2000,
        checkInterval: 4000,
        nodeTimeout: 4000,
        masterTimeout: 4000,
        port: 9999,
        //address: host, //"0.0.0.0"
        // broadcast: networkMark,
        key: key
    });
    // console.log("HOST: " + host);
    // console.log("KEY: " + key);
    self.list = [(host + ":" + myPort)];
    self.add = function(ip, port) {
        var address = ip + ":" + port;
        if (self.list.indexOf(address) < 0) {
            self.list.push(address);
        	self.emit("addNode", address);
        }
    };
    self.remove = function(ip, port) {
        var address = ip + ":" + port;
        var index = self.list.indexOf(address);
        if (index >= 0) {
            self.list.splice(index, 1);
        	self.emit("removeNode", address);
        }
    };

    d.advertise({ port: myPort, host: host});

    //setInterval(function () {
    //    var nano = process.hrtime()[1] / 1000 ;
    //    var milliseconds = (new Date).getTime();
    //
    //    setTimeout(function () {
    //        console.log((process.hrtime()[1] / 1000) - nano );
    //        console.log((new Date).getTime() - milliseconds);
    //        console.log("");
    //    }, 1);
    //}, 1000);


    d.on("promotion", function() {
        // i'm master
    });

    d.on("demotion", function() {
        //not a master any more
    });

    d.on("added", function(obj) {
        // a node started
        self.add(obj.advertisement.host, obj.advertisement.port);
    });

    d.on("removed", function(obj) {
        //a node stoped
        self.remove(obj.advertisement.host, obj.advertisement.port);
    });

    d.on("master", function(obj) {
        //master changed
    });

    d.on("helloReceived", function(obj) {
        // console.log(obj);
        self.add(obj.advertisement.host, obj.advertisement.port); // VERY IMPORTANT: Some time a peer goes up/down fastly, we need to verify 
    });
};


var EventEmitter = require('events').EventEmitter;
util.inherits(PeerConnector, EventEmitter);

PeerConnector.prototype.stop = function() {
    this.d.stop();
};
