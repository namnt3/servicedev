var net = require('net');
var ip = require('ip');
var util = require("util");
const os = require('os');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var WebSocketServer = require('ws').Server;


var peers = new require("./peers");


function getMyAddress(networkAddress, networkMark, cb) {
    var result = {
        host: "",
        port: 1024
    };

    function findPort() {
        var server = net.createServer();
        // console.info("CHECK " + result.port);
        server.listen(result, function(err) {
            server.once('close', function() {
                cb(null, result);
            });
            server.close();
        });
        server.on('error', function(err) {
            result.port += 1;
            findPort();
        })
    }

    var interfaces = os.networkInterfaces();
    for (var key in interfaces) {
        if (interfaces.hasOwnProperty(key)) {
	        for (var i = 0; i < interfaces[key].length; i++) {
	            var item = interfaces[key][i];

	            if (item.family === 'IPv4') {
	                if (item.netmask === networkMark && ip.mask(item.address, item.netmask) === networkAddress) {

	                    result.host = item.address;
	                    findPort();
	                    return;
	                }
	            }
	        }
        }
    }

    cb("NET_WORK>Network is not available. " + networkAddress + "/" + networkMark);	
}


var Server = exports.Server = function(region, service, instance, networkAddress, networkMark) {
    var self = this;
    self.region = region;
    self.service = service;
    self.instance = instance;
    self.networkAddress = networkAddress;
    self.networkMark = networkMark;

    self.key = region + "^" + service + "^" + instance;
    self.peers = {};

    self.express = require('express')();
    self.express.use(cookieParser());

    // parse application/x-www-form-urlencoded
    self.express.use(bodyParser.urlencoded({ extended: false }));
    // parse application/json
    self.express.use(bodyParser.json({
        strict: true
    }));

    self.http = require('http').Server(self.express);
    var wss = self.wss = new WebSocketServer({ server: self.http });
    wss.on('connection', function connection(conn) {
        console.log("New ws client");
        self.emit("newSocketClient", conn);
        //var location = url.parse(ws.upgradeReq.url, true);
        // you might use location.query.access_token to authenticate or share sessions
        // or ws.upgradeReq.headers.cookie (see http://stackoverflow.com/a/16395220/151312)

        //ws.on('message', function incoming(message) {
        //    console.log('received: %s', message);
        //});

    });

    // self.socketio = require('socket.io')(self.http);
    // self.socketio.on('/', function(socket){
    //     self.emit("addClient", socket);
    // });
    //self.socket = sockjs.createServer({ sockjs_url: 'http://cdn.jsdelivr.net/sockjs/1.0.1/sockjs.min.js' });
    //self.socket.on('connection', function(conn) {
        // console.log("New client");;
        //self.emit("newSocketClient", conn);
    //});
    //self.socket.installHandlers(self.http, {prefix:'/__ws'});
};

var EventEmitter = require('events').EventEmitter;
util.inherits(Server, EventEmitter);

Server.prototype.start = function(cb) {
    var self = this;

    getMyAddress(self.networkAddress, self.networkMark, function(err, result) {
        if (err) {
            return cb(err);
        }

        self.express.get("/", function (req, res) {
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify({
                region: self.region,
                service: self.service,
                instance: self.instance,
                peers: self.peers.list
            }));
        });

        self.http.listen(result.port, result.host, 256, function (e) {
            if(e) {
                throw e;
            }

            self.peers = new peers.PeerConnector(result.port, result.host, self.key);

            self.peers.on("addNode", function (info) {
                self.emit("addNode", info);
                self.wss.clients.forEach(function each(client) {
                    client.send("NEW_SERVER");
                });
            });
            self.peers.on("removeNode", function (info)  {
                self.emit("removeNode", info);
            });

            cb(null, result);
        });
    });
};
Server.prototype.preStop = function () {
    this.peers.stop();
};