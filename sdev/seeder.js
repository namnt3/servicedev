var process = require('child_process');
var uuid = require('uuid');
var fs = require("fs");

var hostId;
if (fs.existsSync(__dirname + "/../.hostId")) {
    hostId = fs.readFileSync(__dirname + '/../.hostId', "UTF-8");
}
if (!hostId) {
    hostId = uuid.v4();
    fs.writeFileSync(__dirname + '/../.hostId', hostId, "UTF-8");
}

var conf = JSON.parse(fs.readFileSync(__dirname + '/../conf/server.json', "UTF-8"));
console.log(conf);
console.log(hostId);

for (var region in conf) {
    console.log("Loading region " + region);
    for (var service in conf[region].services) {
        console.log("Loading service " + service);
        runService(service);
    }
}

function runService(name) {
    var appDir = __dirname + '/../services/' + name;
    var child = process.fork(__dirname + '/../sdev/sdev', [appDir, "-c", appDir + "/config.json"], {
        cwd: appDir,
        silent: "pipe"
    });
    child.stdout.on('data', function (data) {
        console.log("[" + name + " STDOUT] " + data);
    });
    child.stderr.on('data', function (data) {
        console.log("[" + name + " STDERR] " + data);
    });
    child.on('exit', function (exitCode) {
        console.log("[" + name + " EXIST] " + exitCode);
    });

    child.on("message", function (msg) {
        if (msg.cmd == "monitor") {
            
            return;
        }
        console.log("[" + name + " MSG] " + JSON.stringify(msg));
    });
}


//if (process.argv.length < 3) {
//    console.error("Missing access token param.");
//    process.exit(1);
//}

