docker run \
  --name mariadb-0 \
  -d \
  -v /home/namnt/servicedev/mariadb-0/config:/etc/mysql/conf.d \
  -v /home/namnt/servicedev/mariadb-0/data:/var/lib/mysql \
  -e MYSQL_INITDB_SKIP_TZINFO=yes \
  -e MYSQL_ROOT_PASSWORD=123456 \
  mariadb\
  --wsrep-new-cluster \
  --wsrep_node_address=$(ip -4 addr ls eth0 | awk '/inet / {print $2}' | cut -d"/" -f1)
