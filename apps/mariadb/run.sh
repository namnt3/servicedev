docker run \
  --name mariadb-0 \
  -d \
  -v /home/namnt/node.projects/servicedev/apps/mariadb/config:/etc/mysql/conf.d \
  -v /home/namnt/node.projects/servicedev/apps/mariadb/data:/var/lib/mysql \
  -e MYSQL_INITDB_SKIP_TZINFO=yes \
  -e MYSQL_ROOT_PASSWORD=123456 \
  -e MYSQL_USER='sdev@%' \
  -e MYSQL_PASSWORD=1234qwer \
  -p 3306:3306 \
  mariadb \
  --wsrep-new-cluster  \
  --wsrep_cluster_address='gcomm://'

#--wsrep_cluster_address='gcomm://'

