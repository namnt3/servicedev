var maria = sdev.service("maria");

module.exports = regionRole;

function regionRole(route, client, input, pluginParams, next) {
    if (!client.userId) {
        return client.cb("ACCESS_DENIED");
    }
    var args = Array.prototype.slice.call(pluginParams);
    var func = args.pop();
    if (typeof func !== "function") {
        console.error(new Error("regionRole(..) takes last arg as a function"));
        client.cb("SYS_ERR>regionRole(..) takes last arg as a function");
        return;
    }
    var regionId = func(input);
    maria.query("SELECT role FROM RegionRole WHERE userId = ? AND regionId=?", [client.userId, regionId], function (err, data) {
        if (err) {
            return client.cb("SYS_ERR>maria>" + err);
        }
        var allRoles = {};
        data.rows.forEach(function (row) {
            allRoles[row.role] = 1;
        });
        for (var i = 0; i < args.length; i++) {
            var requireRole = args[i];
            if(allRoles[requireRole]) {
                return next();
            }
        }
        return client.cb("ACCESS_DENIED>Require region roles : " + args);
    });
}
