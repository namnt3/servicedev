
var tables = require("./sql/tables");

sdev.setModule("utils/aes", require("./utils/aes"));
var maria = sdev.service("maria");

module.exports = init;

function init(cb) {
    console.log("sdev-manager started");
    tables.validate(maria, cb);
}


