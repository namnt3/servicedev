var $ = exports = module.exports = {};

$.string = function (value, cb) {
    var result = true;
    if (value != null && typeof value !== "string") {
        result = false;
    }
    cb(null, result);
};

$.length = function (value, cb) {
    var result = 0;
    if (value != null && typeof value !== "number") {
        result = value.length;
    }
    cb(null, result);
};

$.hasValue = function (value, cb) {
    var result = true;
    if (value === null || value === undefined) {
        result = false;
    } else if (typeof value === "string") {
        if (value.trim() === "") {
            result = false;
        }
    } else if (typeof value === "object") {
        if (value.length === 0) {
            result = false;
        }
    }

    cb(null, result);
};

$.empty = function (value, cb) {
    var result = true;
    if (value === null || value === undefined) {
    } else if (typeof value === "string") {
        if (value.trim() !== "") {
            result = false;
        }
    } else if (typeof value === "object") {
        if (value.length !== 0) {
            result = false;
        }
    }

    cb(null, result);
};

var EMAIL_VALIDTOR = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
$.email = function (value, cb) {
    var result = true;
    if (value && typeof value !== "string") {
        result = false;
    } else {
        value = value.trim();
        if (!EMAIL_VALIDTOR.test(value)) {
            result = false;
        }
    }

    cb(null, result);
};


$.number = function (value, cb) {
    var result = true;
    if (typeof value !== "number") {
        result = false;
    }
    cb(null, result);
};

$.value = function (value, cb) {
    cb(null, value);
};

$.regionId = function (value, cb) {
    var result = true;
    if (value != null && typeof value !== "string") {
        result = false;
    }
    if (/([^a-z0-9\.])\w+/.test(value)) {
        result = false;
    }
    if (value.length < 3 || value.length > 300) {
        result = false;
    }
    cb(null, result);
};