
var bcrypt = require('bcryptjs');
function encrypt(str, cb) {
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(str, salt, function (err, hash) {
            // Store hash in your password DB.
            console.log(hash);
        });
    });
}
console.log(encrypt("admin", function (err, value) {
    console.log(value);
}));