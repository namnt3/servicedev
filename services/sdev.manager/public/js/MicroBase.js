var app = angular.module("MicroBase", ["ui.router", 'angular-clipboard']);

app.service("UrlUtils", function ($rootScope, $state) {
    var s = {};
    s.getValue = function (url, index) {
        var arr = url.split(/\/|\?/);
        if (index < arr.length)
            return arr[index];
        return null;
    };
    s.stateMap = function (map) {
        function check() {
            var d;
            for (var key in map) {
                if (map.hasOwnProperty(key)) {
                    if (key === "$current" || key === "$state") continue;

                    console.log(key + " : " + $state.includes(key));
                    if ($state.includes(key)) {
                        map.$current = map[key];
                        map.$state = key;
                        return;
                    }

                    if (map[key].$default) {
                        d = key;
                    }
                }
            }
            map.$current = map[d];
            map.$state = d;
        }

        $rootScope.$on('$stateChangeSuccess', check);
        check();
    };
    return s;
});

app.service("UserProfile", function (Server, $state, $rootScope) {
    var sv = {
        profile: null
    };
    var user = Server.user;

    sv.getProfile = function (cb) {
        if (sv.profile) {
            var r = {};
            r.__proto__ = sv.profile;
            return cb(null, r);
        }
        user.getProfile(function (err, data) {
            $rootScope.appLoaded = true;
            if (err) {
                if (err.split(">")[0] === "ACCESS_DENIED") {
                    $state.go("login");
                    return;
                }
                cb(err);
                return;
            }
            sv.profile = data;
            cb(null, sv.profile);
        });
    };

    sv.logout = function (cb) {
        user.logout(function (err, data) {
            if (err) {
                cb(err);
            }
            sv.profile = null;
            $state.go("login");
            cb(null);
        });
    };

    return sv;
});

app.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});

app.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/regions");

    $stateProvider
        .state('login', {
            url: "/login",
            templateUrl: "pages/login.html"
        })
        .state('regions', {
            url: "/regions",
            templateUrl: "pages/regions.html"
        })
        .state('region', {
            url: "/region/:regionId",
            templateUrl: "pages/region.html"
        })
        .state('region.overview', {
            url: "/overview",
            templateUrl: "pages/region/overview.html"
        })
        .state('region.services', {
            url: "/services",
            templateUrl: "pages/region/services.html"
        })
        .state('region.service', {
            url: "/service/:serviceId",
            templateUrl: "pages/region/service.html"
        })
        .state('region.service.instances', {
            url: "/instances",
            templateUrl: "pages/region/service/instances.html"
        })
        .state('region.service.status', {
            url: "/status",
            templateUrl: "pages/region/service/status.html"
        })
        .state('region.service.versions', {
            url: "/versions",
            templateUrl: "pages/region/service/versions.html"
        })
        .state('region.service.users', {
            url: "/users",
            templateUrl: "pages/region/service/users.html"
        })
        .state('region.service.settings', {
            url: "/settings",
            templateUrl: "pages/region/service/settings.html"
        })
        .state('region.service.overview', {
            url: "/overview",
            templateUrl: "pages/region/service/overview.html"
        })
        .state('region.users', {
            url: "/users",
            templateUrl: "pages/region/users.html"
        })
        .state('region.servers', {
            url: "/servers",
            templateUrl: "pages/region/server/servers.html"
        })
        .state('region.servers.list', {
            url: "/list",
            templateUrl: "pages/region/server/list.html"
        })
        .state('region.servers.add', {
            url: "/add",
            templateUrl: "pages/region/server/add.html"
        })
        .state('region.servers.add.ssh', {
            url: "/ssh",
            templateUrl: "pages/region/server/add/ssh.html"
        })
        .state('region.servers.add.digitalOcean', {
            url: "/digitalOcean",
            templateUrl: "pages/region/server/add/digitalOcean.html"
        })
        .state('region.server', {
            url: "/server/:serverId",
            templateUrl: "pages/region/server.html"
        })
        .state('region.server.overview', {
            url: "/overview",
            templateUrl: "pages/region/server/overview.html"
        })
        .state('region.server.settings', {
            url: "/settings",
            templateUrl: "pages/region/server/settings.html"
        })
        .state('region.server.console', {
            url: "/console",
            templateUrl: "pages/region/server/console.html"
        })
        .state('region.settings', {
            url: "/settings",
            templateUrl: "pages/region/settings.html"
        })
        .state('region.settings.general', {
            url: "/domains",
            templateUrl: "pages/region/settings/general.html"
        })
        .state('region.settings.domains', {
            url: "/domains",
            templateUrl: "pages/region/settings/domains.html"
        })
        .state('region.settings.delete', {
            url: "/delete",
            templateUrl: "pages/region/settings/delete.html"
        })
        .state('region.settings.clouds', {
            url: "/cloud",
            templateUrl: "pages/region/settings/clouds.html"
        })
});

app.controller("RegionList", function ($scope, UserProfile, Server, $rootScope, $state) {
    $scope.userProfile = {};
    $scope.logout = function () {
        UserProfile.logout(function (err) {
            if (err) {
                console.error(err);
                return;
            }
        });
    };
    $scope.searchText = "";
    $scope.searching = false;
    $scope.validRegionName = false;
    $scope.search = find;
    $scope.create = create;

    $scope.$watch("searchText", function () {
        console.log($scope.searchText);
        $scope.validRegionName = /([^a-z0-9\.])\w+/.test($scope.searchText) == false && $scope.searchText.length >= 3;
        find();
    });

    var findText;

    function find() {
        if ($scope.searching) {
            return;
        }
        $scope.searching = true;
        $scope.regions = [];
        doFind();

        function doFind() {
            findText = $scope.searchText.slice(0);

            Server.region.find({
                text: findText,
                size: 10,
                offset: 0
            }, function (err, regions) {
                if (err) {
                    return console.error(err);
                }
                if (findText === $scope.searchText) {
                    $scope.regions = regions;
                    $scope.searching = false;
                } else {
                    doFind();
                }
            });
        }
    }

    function create() {
        var id = $scope.searchText.slice(0);

        $rootScope.appLoaded = false;
        Server.region.create($scope.searchText, function (err) {
            $rootScope.appLoaded = true;
            if (err) {
                return console.error(err);
            }
            $state.go("region.settings", {regionId: id});
        });
    }

    UserProfile.getProfile(function (err, profile) {
        $rootScope.appLoaded = true;
        if (err) {
            console.error(err);
            return;
        }
        $scope.userProfile = profile;
        console.log($scope.userProfile.icon);
    });

    $scope.regions = [{
        id: "mmsofts.local",
        name: "mmsofts.local",
        users: [{
            avatar: "https://www.gravatar.com/avatar/ceab58f00ee28145094073a82fc343df?s=80&d=retro"
        }, {
            avatar: "https://www.gravatar.com/avatar/ceab58f00ee28145094073a82fc343df?s=80&d=retro"
        }, {
            avatar: "https://www.gravatar.com/avatar/ceab58f00ee28145094073a82fc343df?s=80&d=retro"
        }],
        services: ["mongo", "mysql", "authen", "hadoop", "file.services", "paypal.gateway", "elasticsearch", "mariadb", "eventbus", "activemq"],
        serviceCount: 250,
        userCount: 250
    }, {
        id: "mmsofts.digital_ocean",
        name: "mmsofts.digital_ocean",
        cpu: "8",
        ram: "8GB",
        users: [{
            avatar: "https://www.gravatar.com/avatar/ceab58f00ee28145094073a82fc343df?s=80&d=retro"
        }, {
            avatar: "https://www.gravatar.com/avatar/ceab58f00ee28145094073a82fc343df?s=80&d=retro"
        }, {
            avatar: "https://www.gravatar.com/avatar/ceab58f00ee28145094073a82fc343df?s=80&d=retro"
        }],
        services: ["mongo", "mysql", "authen", "hadoop", "file.services", "paypal.gateway"]
    }, {
        id: "mmsofts.aws",
        name: "mmsofts.aws",
        cpu: "8",
        ram: "8GB",
        users: [{
            avatar: "https://www.gravatar.com/avatar/ceab58f00ee28145094073a82fc343df?s=80&d=retro"
        }, {
            avatar: "https://www.gravatar.com/avatar/ceab58f00ee28145094073a82fc343df?s=80&d=retro"
        }, {
            avatar: "https://www.gravatar.com/avatar/ceab58f00ee28145094073a82fc343df?s=80&d=retro"
        }]
    }, {
        id: "mmsofts.aws",
        name: "mmsofts.aws",
        cpu: "8",
        ram: "8GB",
        users: []
    }, {
        name: "mmsofts.aws",
        cpu: "8",
        ram: "8GB",
        users: [{
            avatar: "https://www.gravatar.com/avatar/ceab58f00ee28145094073a82fc343df?s=80&d=retro"
        }, {
            avatar: "https://www.gravatar.com/avatar/ceab58f00ee28145094073a82fc343df?s=80&d=retro"
        }, {
            avatar: "https://www.gravatar.com/avatar/ceab58f00ee28145094073a82fc343df?s=80&d=retro"
        }]
    }];
});
app.controller("Region", function ($scope, $state, $stateParams, $rootScope, UrlUtils, UserProfile) {
    $scope.profile = {};
    $scope.logout = function () {
        UserProfile.logout(function (err) {
            if (err) {
                console.error(err);
            }
        });
    };

    $scope.menu = {
        "region.overview": {
            goto: "region.overview",
            icon: "fa fa-cubes",
            name: "Overview"
        },
        "region.services": {
            goto: "region.services",
            icon: "fa fa-cubes",
            name: "Services"
        },
        "region.service": {
            goto: "region.services",
            icon: "fa fa-cubes",
            name: "Services"
        },
        "region.users": {
            goto: "region.users",
            icon: "fa fa-users",
            name: "Users",
            link: "../users"
        },
        "region.servers": {
            goto: "region.servers",
            icon: "fa fa-server",
            name: "Servers"
        },
        "region.server": {
            goto: "region.servers",
            icon: "fa fa-server",
            name: "Servers"
        },
        "region.settings": {
            goto: "region.settings",
            icon: "fa fa-cubes",
            name: "Settings"

        }
    };
    UrlUtils.stateMap($scope.menu);

    UserProfile.getProfile(function (err, profile) {
        if (err) {
            console.error(err);
            return;
        }
        $scope.profile = profile;
    });

    $scope.region = {
        id: $stateParams.regionId,
        name: $stateParams.regionId
    };
    $scope.goto = function (state) {
        console.log(state);
        $state.go(state);
    };
    //$scope.path = $state.current.url;
    //console.log($state.current);
    //$scope.state = menus[UrlUtils.getValue($state.current.url, 1)];
    //$scope.info = "";
    //
    //$rootScope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
    //    $scope.state = menus[UrlUtils.getValue(toState.url, 1)];
    //    console.log(UrlUtils.getValue(toState.url, 1));
    //    $scope.info = $state.includes("region.services");
    //});
    //$scope.reloadState = function () {
    //    $state.go($scope.state.path);
    //}
});
app.controller("ServerDetail", function ($scope, $stateParams) {
    //$scope.region = {
    //    id: $stateParams.regionId,
    //    name: $stateParams.regionId
    //}
});
app.controller("Region_Servers", function ($scope, $stateParams) {
    $scope.servers = [{
        id: "server-1",
        name: "server-1",
        ram: 32,
        cpu: 23,
        core: 4,
        disk: 180,
        ip: "10.8.3.5",
        services: ["mongo", "mysql", "authen", "hadoop", "file.services", "paypal.gateway"]
    }, {
        id: "server-3",
        name: "server-3",
        ram: 32,
        cpu: 23,
        core: 4,
        disk: 180,
        ip: "10.8.3.5",
        services: ["mongo", "mysql", "authen", "hadoop", "file.services", "paypal.gateway"]
    }, {
        id: "server-2",
        name: "server-2",
        ram: 32,
        cpu: 23,
        core: 4,
        disk: 180,
        ip: "10.8.3.5",
        services: ["mongo", "mysql", "authen", "hadoop", "file.services", "paypal.gateway"]
    }];
});
app.controller("Region_Services", function ($scope, $stateParams) {
    $scope.services = [{
        name: "user.authenticate",
        icon: "http://www.acsu.buffalo.edu/~abhijitp/img/hadoop.png",
        used: 45,
        providers: [{
            name: "Nam Nguyen"
        }, {
            name: "Kim Nguyen"
        }],
        users: [{
            name: "Nam Nguyen"
        }, {
            name: "Kim Nguyen"
        }]
    }, {
        name: "mysqldb",
        icon: "https://lh3.googleusercontent.com/-zs50e2HtyGY/UGEcqsjHJwI/AAAAAAAAAJA/Yn3eKvPvhKY/s800/mysql_logo.png",
        used: 16,
        providers: [{
            name: "Nam Nguyen"
        }, {
            name: "Canh Nguyen"
        }],
        users: [{
            name: "Nam Nguyen"
        }, {
            name: "Kim Nguyen"
        }]
    }];
});
app.controller("RegionUsers", function ($scope, $stateParams) {
});
app.controller("Region_Server_Users", function ($scope, $stateParams) {
    $scope.$stateParams = $stateParams;
    $scope.where = "region server lists";
});
app.controller("Region_Service", function ($scope, $stateParams) {
    $scope.$stateParams = $stateParams;
    $scope.where = "Service detail";
});

app.controller("Region_Service_Overview", function ($scope, $stateParams) {
    $scope.$stateParams = $stateParams;
    $scope.where = "Region_Service_Overview";

    drawChart();
});
app.controller("Region_Service_Instances", function ($scope, $stateParams) {
    $scope.$stateParams = $stateParams;
    $scope.where = "Instances";
});
app.controller("Region_Service_Versions", function ($scope, $stateParams) {
    $scope.$stateParams = $stateParams;
    $scope.where = "Region_Service_Versions";
});
app.controller("Region_Service_Users", function ($scope, $stateParams) {
    $scope.$stateParams = $stateParams;
    $scope.where = "Region_Service_Users";
});
app.controller("Region_Service_Settings", function ($scope, $stateParams) {
    $scope.$stateParams = $stateParams;
    $scope.where = "Region_Service_Settings";
});
app.controller("Region_Service_Status", function ($scope, $stateParams) {
    $scope.$stateParams = $stateParams;
    $scope.where = "Region_Service_Status";
});
app.controller("Region_Overview", function ($scope, $stateParams) {
    $scope.$stateParams = $stateParams;
    $scope.where = "Region_Overview";
});
app.controller("Region_Server", function ($scope, $stateParams) {
    $scope.$stateParams = $stateParams;
    $scope.where = "Region_Server";
});
app.controller("Region_Server_Overview", function ($scope, $stateParams) {
    $scope.$stateParams = $stateParams;
    $scope.where = "Region_Server_Overview";
});

app.controller("Region_Settings", function ($scope, $stateParams) {
    $scope.$stateParams = $stateParams;
    $scope.where = "Region_Settings";
});
app.controller("Region_Settings_Domains", function ($scope, $stateParams) {
    $scope.$stateParams = $stateParams;
    $scope.where = "Region_Settings_Domains";
});
app.controller("Region_Settings_General_SdSeeder", function ($scope, $stateParams, Server, clipboard) {
    $scope.at = "";

    $scope.formLoading = false;
    Server.region.getAccessToken($stateParams.regionId, function (err, result) {
        if (err) {
            return console.error(err);
        }
        $scope.at = result;
    });
    $scope.copyToClipboard = function () {
        if (!clipboard.supported) {
            return alert("Sorry, your browser doesn't support copying");
        }
        clipboard.copyText($scope.at);
    }
});
app.controller("Region_Settings_General_Network", function ($scope, $stateParams, Server, clipboard) {
    $scope.form = {};
    $scope.formLoading = false;
    Server.region.getNetworkInfo($stateParams.regionId, function (err, result) {
        if (err) {
            return console.error(err);
        }
        $scope.form = result;
    });

    $scope.save = function () {
        $scope.formLoading = true;
        Server.region.saveNetworkInfo($stateParams.regionId, $scope.form, function (err) {
            $scope.formLoading = false;
            if (err) {
                $scope.fromErr = err;
                return;
            }
        });
    };
});

app.controller("Region_Settings_Delete", function ($stateParams, $scope, $rootScope, $state, Server) {
    $scope.password = "";

    $scope.formLoading = false;
    $scope.deleteRegion = function () {
        if (!$scope.password) {
            $scope.passwordError = true;
            return;
        }
        $scope.formLoading = true;
        $scope.passwordError = false;
        //console.log("NICE");
        //console.log();
        Server.region.delete($stateParams.regionId, $scope.password, function (err, result) {
            $scope.formLoading = false;
            if (err) {
                if (err.split(">")[0] === "INVALID_PASS") {
                    $scope.passwordError = true;
                }
                return console.error(err);
            }
            $state.go("regions");
        });
    };
});
app.controller("Region_Settings_Clouds", function ($stateParams, $scope, $rootScope, $state, Server) {
});
app.controller("Region_Settings_General", function ($scope, $stateParams) {
    $scope.$stateParams = $stateParams;
    $scope.where = "Region_Server_Settings";
});

app.controller("Region_Server_Settings", function ($scope, $stateParams) {
    $scope.$stateParams = $stateParams;
    $scope.where = "Region_Server_Settings";
});

app.controller("Region_Server_Console", function ($scope, $stateParams) {
    $scope.$stateParams = $stateParams;
    $scope.where = "Region_Server_Console";
});
app.controller("Region_Server_Overview", function ($scope, $stateParams) {
    $scope.$stateParams = $stateParams;
    $scope.where = "Region_Server_Overview";
});
app.controller("Region_Servers_Add_Ssh", function ($scope, $stateParams) {
    $scope.$stateParams = $stateParams;
    $scope.where = "Region_Servers_Add_Ssh";
});
app.controller("Region_Servers_Add_Digital_Ocean", function ($scope, $stateParams) {
    $scope.$stateParams = $stateParams;
    $scope.where = "Region_Servers_Add_Digital_Ocean";
});
app.controller("login", function ($scope, Server, $state, UserProfile) {
    var user = Server.user;
    UserProfile.getProfile(function (err, data) {
        if (err) {
            if (err.split(">")[0] === "ACCESS_DENIED") {
                return;
            }
            console.error(err);
            return;
        }
        $state.go("regions");
    });
    $scope.form = {
        loading: false
    };
    $scope.login = function () {
        if (!$scope.form.email || !$scope.form.password) {
            $scope.form.error = "Email & password must be provided.";
            return;
        }
        $scope.form.loading = true;
        $scope.form.error = "";
        user.login({
            email: $scope.form.email,
            password: $scope.form.password
        }, function (err, data) {
            $scope.form.loading = false;
            if (err) {
                if (err.split(">")[0] === "WRONG_PASS") {
                    $scope.form.error = "Invalid email or password.";
                    return;
                }
                $scope.form.error = err;
                return;
            }
            $state.go("regions");
        });
    };
});


function drawChart() {
    $(function () {
        $('#container').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Fruit Consumption'
            },
            xAxis: {
                categories: ['Apples', 'Bananas', 'Oranges']
            },
            yAxis: {
                title: {
                    text: 'Fruit eaten'
                }
            },
            series: [{
                name: 'Jane',
                data: [1, 0, 4]
            }, {
                name: 'John',
                data: [5, 7, 3]
            }]
        });
    });

    /*global $*/
    /*global Highcharts*/
    $(document).ready(function () {
        Highcharts.setOptions({
            global: {
                useUTC: false
            }
        });

        $('#requestPerSecond').highcharts({
            chart: {
                type: 'spline',
                spacingTop: 30,
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function () {

                        // set up the updating of the chart each second
                        var series = this.series[0];
                        setInterval(function () {
                            var x = (new Date()).getTime(), // current time
                                y = Math.round(Math.random() * 1000) + 2000;
                            series.addPoint([x, y], true, true);
                        }, 1000);
                    }
                }
            },
            title: {
                text: "3000 Request/s"
            },
            xAxis: {
                crosshair: true,
                type: 'datetime',
                tickPixelInterval: 9000
            },
            yAxis: {
                title: {
                    text: null
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: 'red'
                }]
            },
            tooltip: {
                positioner: function () {
                    return {
                        x: this.chart.chartWidth - this.label.width, // right aligned
                        y: -1 // align to title
                    };
                },
                borderWidth: 0,
                backgroundColor: 'none',
                pointFormat: '{point.y}',
                headerFormat: '',
                shadow: false,
                style: {
                    fontSize: '18px'
                }
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [{
                name: 'Request per second',
                data: (function () {
                    // generate an array of random data
                    var data = [],
                        time = (new Date()).getTime(),
                        i;

                    for (i = -19; i <= 0; i += 1) {
                        data.push({
                            x: time + i * 1000,
                            y: Math.random()
                        });
                    }
                    return data;
                }())
            }]
        });
    });
}