var async = require("async");

module.exports = Sql;

function Sql() {
    this.tables = {};
}

Sql.prototype.table = function (name) {
    if (!this.tables[name]) {
        this.tables[name] = new Table(name);
    }
    return this.tables[name];
};

Sql.prototype.validateStructure = function (maria, cb) {
    async.forEachSeries(this.tables, function (table, next) {
        console.log("Table[" + table.name + "]");
        maria.query(table.getSql(), [], function (err) {
            if (err) {
                return next(err);
            }
            maria.query("SHOW COLUMNS FROM `" + table.name + "`", function (err, data) {
                if (err) {
                    return next(err);
                }
                var fields = {};
                data.rows.forEach(function (row) {
                    fields[row.Field] = 1;
                });
                async.forEachSeries(table.fields, function (field, next) {
                    if (fields[field.name]) {
                        maria.query("ALTER TABLE `" + table.name + "` MODIFY COLUMN " + field.sql, [], next);
                    } else {
                        maria.query("ALTER TABLE `" + table.name + "` ADD COLUMN " + field.sql, [], next);
                    }
                }, next);
            });
        });
    }, function (err) {
        if (err) {
            console.error(err);
        }
        cb(err);
    });
};
Sql.prototype.insert = function (maria, cb) {
    async.forEachSeries(this.tables, function (table, next) {
        if (table.data.length === 0) {
            return next();
        }
        async.forEachSeries(table.data, function (obj, next) {
            maria.query("INSERT INTO `" + table.name + "` SET ?", obj, function (error) {
                console.log("INSERT FAIL " + error);
                next();
            });
        }, next);
    }, function (err) {
        if (err) {
            console.error(err);
        }
        cb(err);
    });
};
Sql.prototype.validate = function (maria, cb) {
    var self = this;
    this.validateStructure(maria, function (err) {
        if (err) {
            return cb(err);
        }
        self.insert(maria, cb);
    });
};

function Table(name) {
    this.name = name;
    this.fields = [];
    this.primaryKeys = null;
    this.engineName = "InnoDb";
    this.data = [];
}
Table.prototype.field = function () {
    this.fields.push({
        name: arguments[0],
        sql: Array.prototype.slice.call(arguments).join(" ")
    });
    return this;
};
Table.prototype.getSql = function () {
    var str = "CREATE TABLE IF NOT EXISTS `" + this.name + "` (" + "\n";
    this.fields.forEach(function (field) {
        str += "\t" + field.sql + ",\n";
    });
    str += "\tPRIMARY KEY (" + this.primaryKeys + ")\n";
    str += ") ENGINE=" + this.engineName + ";";
    return str;
};

Table.prototype.primaryKey = function () {
    var value;
    Array.prototype.slice.call(arguments).forEach(function (field) {
        if (value) {
            value += ", `" + field + "`";
        } else {
            value = "`" + field + "`";
        }
    });
    this.primaryKeys = value;
    return this;
};
Table.prototype.engine = function (name) {
    this.engineName = "'" + name + "'";
    return this;
};
Table.prototype.insert = function (obj) {
    this.data.push(obj);
    return this;
};