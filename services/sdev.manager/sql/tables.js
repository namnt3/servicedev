var Sql = require("./sql");

var sql = module.exports = new Sql();

sql.table("User")
    .field("id", "BIGINT", "AUTO_INCREMENT")
    .field("password", "NVARCHAR(1024)")
    .field("email", "NVARCHAR(50)")
    .field("name", "NVARCHAR(50)")
    .field("icon", "NVARCHAR(1024)")
    .primaryKey("id")
    .engine("InnoDb");

sql.table("User").insert({
    id: 1,
    password: "$2a$10$vxsDAtSpN3tAbBEd3UZtP.8Lpd7gyDJDUXY2nJ6lKia1zUgho0iUq",
    email: "admin@servicedev.net",
    name: "servicedev admin",
    icon: "https://www.brightfind.com/images/services/dev/icon-dev-checkmark.png"
});


sql.table("RegionRole")
    .field("userId", "BIGINT", "NOT NULL")
    .field("regionId", "VARCHAR(50)", "NOT NULL")
    .field("role", "VARCHAR(50)")
    .primaryKey("userId", "regionId", "role")
    .engine("InnoDb");

sql.table("ServiceRole")
    .field("userId", "BIGINT", "NOT NULL")
    .field("regionId", "VARCHAR(50)", "NOT NULL")
    .field("serviceId", "VARCHAR(50)", "NOT NULL")
    .field("role", "VARCHAR(50)")
    .primaryKey("userId", "regionId", "serviceId", "role")
    .engine("InnoDb");

sql.table("Region")
    .field("id", "VARCHAR(50)", "NOT NULL")
    .field("icon", "NVARCHAR(1024)")
    .field("network", "VARCHAR(100)", "NOT NULL")
    .field("domains", "TEXT")
    .primaryKey("id")
    .engine("InnoDb");

sql.table("Service")
    .field("id", "VARCHAR(50)", "NOT NULL")
    .field("regionId", "VARCHAR(50)", "NOT NULL")
    .field("icon", "NVARCHAR(1024)")
    .primaryKey("regionId", "id")
    .engine("InnoDb");

sql.table("Instance")
    .field("id", "VARCHAR(50)", "NOT NULL")
    .field("regionId", "VARCHAR(50)", "NOT NULL")
    .field("serviceId", "VARCHAR(50)", "NOT NULL")
    .primaryKey("regionId", "serviceId", "id")
    .engine("InnoDb");

sql.table("Resource")
    .field("id", "VARCHAR(50)", "NOT NULL")
    .field("regionId", "VARCHAR(50)", "NOT NULL")
    .field("serviceId", "VARCHAR(50)", "NOT NULL")
    .field("instanceId", "VARCHAR(50)", "NOT NULL")
    .field("accessToken", "VARCHAR(300)", "")
    .primaryKey("regionId", "serviceId", "instanceId", "id")
    .engine("InnoDb");


sql.table("Server")
    .field("id", "VARCHAR(50)", "NOT NULL")
    .field("regionId", "VARCHAR(50)", "NOT NULL")
    .field("core", "INT", "NOT NULL")
    .field("cpu", "INT", "NOT NULL")
    .field("ram", "INT", "NOT NULL")
    .field("disk", "BIGINT")
    .field("iif", "VARCHAR(100)")
    .primaryKey("id")
    .engine("InnoDb");


sql.table("ServiceStatus")
    .field("regionId", "VARCHAR(50)", "NOT NULL")
    .field("serviceId", "VARCHAR(50)", "NOT NULL")
    .field("time", "BIGINT", "NOT NULL")
    .field("route", "VARCHAR(100)", "NOT NULL")
    .field("request", "INT", "DEFAULT 0")
    .field("process", "INT", "DEFAULT 0")
    .field("lwt", "BIGINT", "DEFAULT 0")
    .primaryKey("regionId", "serviceId", "time")
    .engine("InnoDb");