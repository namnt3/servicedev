var maria = sdev.service("maria");
var async = require("async");
var bcrypt = require('bcryptjs');

var aes = sdev.module("utils/aes");

sdev.route("find")
    .authenticated()
    .valid({
        text: "string",
        size: "number && value > 0 && value < 100",
        offset: "number && value >= 0"
    })
    .run(function (client, query) {
        var text = query.text;
        text = text.trim().split(" ");
        text = "%" + text.join("%") + "%";
        maria.query("SELECT regionId as id FROM RegionRole WHERE regionId LIKE ? AND userId=? LIMIT ? OFFSET ?",
            [text, client.userId, query.size, query.offset],
            function (err, data) {
                if (err) {
                    return client.cb("SYS_ERR>" + err);
                }
                var regions = data.rows;
                async.forEachSeries(regions, function (region, next) {
                    maria.query("SELECT DISTINCT(userId) FROM `RegionRole` WHERE regionId=? LIMIT 8;", [region.id],
                        function (err, data) {
                            if (err) {
                                return next(err);
                            }
                            region.users = [];
                            data.rows.forEach(function (row) {
                                region.users.push(row.userId);
                            });
                            next();
                        });
                }, function (err) {
                    setTimeout(function () {
                        client.cb(err, regions);
                    }, 500);
                });
            });
    });

sdev.route("delete")
    .authenticated()
    .valid("regionId", "string && hasValue")
    .regionRole("ADMIN", function (input) {
        return input[0];
    })
    .run(function (client, name, password) {
        maria.query("SELECT password FROM `User` WHERE id=?", client.userId, function (err, data) {
            if (err) {
                console.error(err);
                return client.cb("SYS_ERR> got database error: " + err);
            }
            if (data.rows.length === 0) {
                return client.cb("USER_NOT_FOUND");
            }
            compare(password, data.rows[0].password, function (err, correct) {
                if (err || !correct) {
                    return client.cb("INVALID_PASS");
                }

                maria.query("DELETE FROM Region WHERE id = ?; DELETE FROM RegionRole WHERE regionId=?", [name, name], function (err) {
                    if (err) {
                        console.error(err);
                        return client.cb("SYS_ERR> got database error: " + err);
                    }
                    client.cb();
                });
            });
        });
    });

sdev.route("create")
    .authenticated()
    .valid("regionId")
    .run(function (client, regionId) {
        maria.query("INSERT INTO Region SET ?", {
            id: regionId,
            network: "127.0.0.0/255.0.0.0"
        }, function (err, data) {
            if (err) {
                return client.cb("SYS_ERR>" + err);
            }
            maria.query("INSERT INTO RegionRole SET ?",
                {
                    userId: client.userId,
                    regionId: regionId,
                    role: "ADMIN"
                },
                function (err, data) {
                    setTimeout(function () {
                        if (err) {
                            return client.cb("SYS_ERR>" + err);
                        }
                        client.cb(null);
                    }, 2000);
                });
        });
    });

sdev.route("getAccessToken")
    .authenticated()
    .valid("regionId")
    .regionRole("ADMIN", function (input) {
        return input[0];
    })
    .run(function (client, regionId) {
        client.cb(null, aes.encrypt(regionId));
    });

sdev.route("getNetworkInfo")
    .authenticated()
    .valid("regionId")
    .regionRole("ADMIN", function (input) {
        return input[0];
    })
    .run(function (client, regionId) {
        maria.query("SELECT network FROM Region WHERE id=?", [regionId], function (err, data) {
            if (err) {
                return client.cb("SYS_ERR>" + err);
            }
            if (data.rows.length > 0) {
                return client.cb(null, data.rows[0]);
            }
            client.cb("NOT_FOUND>");
        });
    });

sdev.route("getNetworkInfo")
    .authenticated()
    .valid("regionId")
    .regionRole("ADMIN", function (input) {
        return input[0];
    })
    .run(function (client, regionId) {
        maria.query("SELECT network,domains FROM Region WHERE id=?", [regionId], function (err, data) {
            if (err) {
                return client.cb("SYS_ERR>" + err);
            }
            if (data.rows.length > 0) {
                return client.cb(null, data.rows[0]);
            }
            client.cb("NOT_FOUND>");
        });
    });

sdev.route("saveNetworkInfo")
    .authenticated()
    .valid("regionId", {
        network: "string && length > 10 && length < 100",
        domains: "string"
    })
    .regionRole("ADMIN", function (input) {
        return input[0];
    })
    .run(function (client, regionId, info) {
        maria.query("UPDATE Region SET ? WHERE id = ?", [{
            network: info.network,
            domains: info.domains
        }, regionId], function (err, data) {
            if (err) {
                return client.cb("SYS_ERR>" + err);
            }
            client.cb();
        });
    });


function compare(str, hash, cb) {
    bcrypt.compare(str, hash, function (err, res) {
        cb(err, res);
    });
}

function encrypt(str, cb) {
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(str, salt, function (err, hash) {
            // Store hash in your password DB.
            cb(err, hash);
        });
    });
}