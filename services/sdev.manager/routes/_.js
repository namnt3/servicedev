var maria = sdev.service("maria")
var echo = maria.test.MyEcho.echo;


sdev.route("")
    .html(function (client) {
        client.redirect("public/index.html");
    });

sdev.route("echo")
    .valid("string && hasValue")
    .run(function (client, msg) {
        // console.log("MSG > " + msg);

        echo(msg, function (err, msg) {
            client.cb(err, msg);
        });
    });

sdev.route("pid")
    .run(function (client, msg) {
        client.cb(null, process.pid);
    });