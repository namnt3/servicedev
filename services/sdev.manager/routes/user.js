var bcrypt = require('bcryptjs');

var db = sdev.service("maria");

sdev.route("login")
    .valid({
        email: "string && !empty",
        password: "string && !empty"
    })
    .run(function (client, msg) {

        db.query("SELECT password,id FROM `User` WHERE email=?", msg.email, function (err, data) {
            if (err) {
                console.error(err);
                return client.cb("SYS_ERR> got database error: " + err);
            }
            var rows = data.rows;
            if (rows.length === 0) {
                setTimeout(function () {
                    client.cb("WRONG_PASS>");
                }, 2000);
                return;
            }
            compare(msg.password, rows[0].password, function (err, correct) {
                if (correct) {
                    client.userId = rows[0].id;
                    console.log(client.userId);
                    return client.cb();
                }
                client.cb("WRONG_PASS>");
            });
        });
    });

sdev.route("getProfile")
    .run(function (client) {
        if (!client.userId) {
            return client.cb("ACCESS_DENIED>");
        }
        db.query("SELECT * FROM `User` WHERE id=?", client.userId, function (err, data) {
            if (err) {
                console.error(err);
                return client.cb("SYS_ERR> got database error: " + err);
            }
            if (data.rows.length > 0) {
                setTimeout(function () {
                    client.cb(null, data.rows[0]);
                }, 2000);
            } else {
                client.cb(null);
            }
        });
    });

sdev.route("logout")
    .run(function (client) {
        client.userId = undefined;
        client.cb();
    });

function encrypt(str, cb) {
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(str, salt, function (err, hash) {
            // Store hash in your password DB.
            console.log(hash);
        });
    });
}

function compare(str, hash, cb) {
    bcrypt.compare(str, hash, function (err, res) {
        cb(err, res);
    });
}