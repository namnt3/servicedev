var mysql = require('mysql');
var db = module.exports = {};
db.cons = {};
db.get = function (name) {
    if(db.cons[name]) {
        return db.cons[name];
    }
    return db.cons[name] = mysql.createPool({
        connectionLimit : 3,
        host            : 'localhost',
        user            : 'root',
        password        : '123456',
        database        : name,
        trace           : false,
        multipleStatements: true
    });
};

db.close = function (name, cb) {
    var pool = db.cons[name];
    delete db.cons[name];

    if(pool) {
        return pool.end(cb);
    }
    cb();
};
