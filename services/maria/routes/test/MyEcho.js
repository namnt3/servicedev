sdev.route("echo")
    .run(function (client, msg) {
        console.log(client.userId + ": " + msg);
        client.cb(null, msg);
    });