var db = sdev.module("db");

sdev.route("")
    .html(function (client) {
        db.get("maria").query('SELECT table_schema "name", '
            + 'Round(Sum(data_length + index_length) / 1024 / 1024, 1) "size" '
            + 'FROM   information_schema.tables '
            + 'GROUP  BY table_schema;', function (err, result) {
            if (err) {
                console.error(err);
                client.cb("DB_ERR>" + err);
                return;
            }
            var dbs = [];
            dbs.select = function (name) {
                for (var i = 0; i < dbs.length; i++) {
                    if (dbs[i].name === name) {
                        return dbs[i];
                    }
                }
                return null;
            };
            result.forEach(function (item) {
                dbs.push({
                    name: item.name,
                    size: item.size,
                    deletable: false
                })
            });

            db.get("maria").query("SELECT id FROM `User` ORDER BY id", function (err, rows) {
                if (err) {
                    console.error(err);
                    client.cb("DB_ERR>" + err);
                    return;
                }
                rows.forEach(function (item) {
                    var obj = dbs.select(item.id);
                    if (obj) {
                        obj.deletable = true;
                    } else {
                        dbs.push({
                            name: item.id,
                            size: 0,
                            deletable: true
                        });
                    }

                });

                console.log(dbs);
                client.render("index", {
                    dbs: dbs
                });
            });
        });
    });

sdev.route("query")
    .valid("string")
    .run(function (client, query, values) {
        if (!client.userId) {
            client.cb("ACCESS_DENIED>");
        }
        values = values || [];
        console.log(query);
        console.log(values);
        db.get(client.userId).query(query, values, function (err, rows, fields) {
            if (err) {
                console.error(err);
                return client.cb("SQL_ERR>" + err);
            }
            client.cb(null, {
                rows: rows,
                fields: fields
            });
        });
    });

sdev.route("create")
    .valid({
        id: "string && length > 2 && length < 20"
    })
    .run(function (client, form) {
        db.get("maria").query("CREATE DATABASE `" + form.id + "`", function (err) {
            if (err) {
                if (err.code === "ER_DB_CREATE_EXISTS") {
                    client.cb("EXIST>User already exists");
                    return;
                }
                console.error(err);
                client.cb("DB_ERR> " + err);
                return;
            }

            db.get("maria").query("INSERT INTO `User` SET ?", form, function (err) {
                if (err) {
                    if (err.code !== 'ER_DUP_ENTRY') {
                        console.error(err);
                        client.cb("DB_ERR> " + err);
                        return;
                    }
                }

                client.userId = form.id;
                client.cb();
            });
        });
    });

sdev.route("delete")
    .valid({
        id: "string && length > 2 && length < 20"
    })
    .run(function (client, obj) {
        var userId = obj.id;
        db.get("maria").query("DROP DATABASE `" + userId + "`", function (err) {
            if (err) {
                console.error(err);
                client.cb("DB_ERR> " + err);
                return;
            }
            db.close(userId, function () {
            });

            db.get("maria").query("DELETE FROM `User` WHERE id=?", [userId], function (err) {
                if (err) {
                    console.error(err);
                    client.cb("DB_ERR> " + err);
                    return;
                }
                client.cb();
            });
        });
    });

sdev.route('user')
    .run(function (client, obj) {
        if (!obj || !obj.userId) {
            return client.cb(null, "You are " + client.userId);
        }
        client.userId = obj.userId;
        return client.cb(null, "You are now " + client.userId);
    });