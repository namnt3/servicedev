sdev.setModule("db", require("./libs/db"));

var db = sdev.module("db");

module.exports = init;

function verifyDb(cb) {
    db.get("mysql").query("CREATE DATABASE maria", function (err) {
        if (err) {
            if (err.code !== 'ER_DB_CREATE_EXISTS') {
                console.error(err);
                cb(err);
                return;
            }
        } else {
            console.log("CREATED DATABASE maria");
        }
        var query = "CREATE TABLE `User` ("
            + "`id` VARCHAR(255) NOT NULL PRIMARY KEY,"
            + "rows VARCHAR(255)"
            + ") engine=InnoDB";

        db.get("maria").query(query, function (err) {
            if (err) {
                if (err.code !== "ER_TABLE_EXISTS_ERROR") {
                    console.error(err);
                    cb(err);
                    return;
                }
            } else {
                console.log("CREATED TABLE User");
            }
            cb();
        });
    });
}

function init(cb) {
    db.get("mysql").query('SELECT 1 + 1 AS solution', function (err, rows, fields) {
        if (err) {
            console.error(err);
            cb("DB_CONNECTION>Can't open connection to sql!")
        }
        console.log("[SQLDB] connected");
        verifyDb(cb);
    });
}